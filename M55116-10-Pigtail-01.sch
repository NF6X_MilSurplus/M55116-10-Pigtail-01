EESchema Schematic File Version 2
LIBS:Audio
LIBS:Battery
LIBS:Connectors
LIBS:Displays
LIBS:ESD
LIBS:Interface
LIBS:LEDs
LIBS:LightBlueBean
LIBS:Logic
LIBS:Memory
LIBS:PIC12F
LIBS:PIC16F
LIBS:PIC18F
LIBS:Potentiometers
LIBS:Power
LIBS:PMIC
LIBS:RLC
LIBS:RTC
LIBS:STM32F
LIBS:Switches
LIBS:Tag-Connect
LIBS:Transistors
LIBS:Xilinx
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:M55116-10-Pigtail-01-cache
EELAYER 25 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "M55116/10 Flex Pigtail 01"
Date "2017-07-10"
Rev "v1"
Comp "Mark J. Blair NF6X"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L M55116-10 J1
U 1 1 5964775E
P 6200 3800
F 0 "J1" H 6200 4150 60  0000 C CNN
F 1 "M55116-10" H 6200 3450 60  0000 C CNN
F 2 "NF6X_Connectors:M55116-10" H 6200 3350 60  0001 C CNN
F 3 "" H 6350 3850 60  0001 C CNN
	1    6200 3800
	1    0    0    -1  
$EndComp
$Comp
L CONN_02X03 J2
U 1 1 5964842E
P 5050 4400
F 0 "J2" H 5050 4600 50  0000 C CNN
F 1 "CONN_02X03" H 5050 4200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 5050 3200 50  0001 C CNN
F 3 "" H 5050 3200 50  0000 C CNN
	1    5050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4300 4700 4300
Wire Wire Line
	4700 4300 4700 3550
Wire Wire Line
	4700 3550 5950 3550
Wire Wire Line
	5300 4300 5400 4300
Wire Wire Line
	5400 4300 5400 3650
Wire Wire Line
	5400 3650 5950 3650
Wire Wire Line
	5950 3750 4600 3750
Wire Wire Line
	4600 3750 4600 4400
Wire Wire Line
	4600 4400 4800 4400
Wire Wire Line
	5950 3850 5500 3850
Wire Wire Line
	5500 3850 5500 4400
Wire Wire Line
	5500 4400 5300 4400
Wire Wire Line
	5950 3950 4500 3950
Wire Wire Line
	4500 3950 4500 4500
Wire Wire Line
	4500 4500 4800 4500
Wire Wire Line
	5950 4050 5600 4050
Wire Wire Line
	5600 4050 5600 4500
Wire Wire Line
	5600 4500 5300 4500
Text Label 5750 3550 0    60   ~ 0
A
Text Label 5750 3650 0    60   ~ 0
B
Text Label 5750 3750 0    60   ~ 0
C
Text Label 5750 3850 0    60   ~ 0
D
Text Label 5750 3950 0    60   ~ 0
E
Text Label 5750 4050 0    60   ~ 0
F
$EndSCHEMATC
