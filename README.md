# M55116-10-Pigtail-01
A flex cable adapting an M55116/10 connector to a 2x3 header.

Created for the OSH Park flex service test run.

M55116/10 is a panel-mount 6-pin audio/data receptacle commonly used
on US military radios, starting with the earlier 5-pin variant in the
1960s. This flex cable adapts it to a 6 pin header, to simplfy cabling
it to a circuit card. While this is normally done with a PC pin
variant of the connector, this flex cable is designed for use with the
more common solder cup variant.
